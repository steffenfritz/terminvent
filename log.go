package terminvent

import "log"

// LogIt writes a message to stdout
func LogIt(msgType string, funcName string, msg string) {
	log.Println(msgType + " " + funcName + " " + msg)
}
