package terminvent

import (
	"context"
	"log"
	"time"

	nmap "github.com/Ullaakut/nmap/v2"
)

// SimpleScan scans a network in CIDR notation for hosts using nmap
func SimpleScan(cidr string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	scanner, err := nmap.NewScanner(
		nmap.WithTargets(cidr),
		nmap.WithContext(ctx),
	)
	if err != nil {
		return err
	}

	result, warnings, err := scanner.Run()
	if err != nil {
		return err
	}

	if warnings != nil {
		log.Printf("Warnings: \n %v", warnings)
	}

	for _, host := range result.Hosts {
		log.Println(host)
	}
	return nil
}
