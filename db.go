package terminvent

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

// ConnectDB connects to a sqlite db and returns
// an error type and a sql.DB type for global use
// close the connection in your main function
func ConnectDB(dbName string) (error, *sql.DB) {

	db, err := sql.Open("sqlite3", dbName)

	return err, db
}

// CreateDB creates a sqlite database for terminvent
// and adds some default values and metadata
func CreateDB(dbName string) error {
	err, db := ConnectDB(dbName + "?_foreign_keys=true")

	if err != nil {
		return err
	}
	sqlStmt := `
		create table terminvent (tiversion TEXT not null, 
			datecreated TEXT not null,
			servername TEXT,
			servercert TEXT,
			serverkey TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table manufacturer (id integer not null primary key,
		uuid TEXT not null,
		name TEXT not null,
		country TEXT,
		city TEXT,
		street TEXT,
		number TEXT,
		addressnote TEXT,
		oid TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table devices (id integer not null primary key,
		uuid TEXT not null,
		description TEXT,
		serialNumber TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table netinterface (id integer not null primary key,
		uuid TEXT not null,
		description TEXT,
		mac TEXT,
		ip TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table networks (id integer not null primary key,
		uuid TEXT not null,
		description TEXT,
		name TEXT,
		zone TEXT,
		gateway0 TEXT,
		gateway1 TEXT,
		router0 TEXT,
		router1 TEXT,
		netmask TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table softwares (id integer not null primary key,
		uuid TEXT not null,
		description TEXT,
		task TEXT,
		name TEXT,
		version TEXT,
		patchlevel TEXT,
		platform TEXT,
		eol INTEGER);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table snmps (id integer not null primary key,
		uuid TEXT not null,
		description TEXT,
		OID TEXT,
		value TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}

	sqlStmt = `
		create table systems (id integer not null primary key,
		uuid TEXT not null,
		devices_uuid TEXT,
		foreign_uuid TEXT,
		foreign_type TEXT);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		return err
	}
	return err
}
