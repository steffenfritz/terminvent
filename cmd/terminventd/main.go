/*Copyright (C) 2021  Steffen Fritz <steffen@fritz.wtf>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*/
package main

import (
	ti "codeberg.org/steffenfritz/terminvent"
	flag "github.com/spf13/pflag"
)

func main() {
	netScan := flag.StringP("netscan", "s", "", "Scan the provided CIDR network for devices")
	createDB := flag.StringP("initdb", "i", "", "Create new sqlite database")

	flag.Parse()

	if len(*netScan) != 0 {
		err := ti.SimpleScan(*netScan)
		if err != nil {
			ti.LogIt("error", "netScan", err.Error())
		}
	}

	if len(*createDB) != 0 {
		err := ti.CreateDB(*createDB)
		if err != nil {
			ti.LogIt("error", "createDB", err.Error())
		}
	}

}
